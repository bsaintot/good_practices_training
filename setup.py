from setuptools import find_packages, setup

setup(
    name='Good Practices Training',
    author='TOTAL',
    description='A data science project',
    packages=find_packages(),
    setup_requires=['setuptools_scm'],
    python_requires='~=3.7',
    install_requires=['python-dotenv',
                      # example librairies
                      'streamlit', 'pandas', 'numpy', 'altair'],
    extras_require={'tests': ['pytest==5.0.1']},
    version='0.1.0'
)
